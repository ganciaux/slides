import os
import sys

dirname = os.path.dirname(__file__)
print(dirname)
sys.path.append(dirname)

c = get_config()
c.Exporter.template_file = 'default_transition'
c.Exporter.template_path.append(dirname)
c.Exporter.preprocessors = ['execute_markdown.ExecutePreprocessor']
