#!/usr/bin/env python3

import smtplib
from email.mime.text import MIMEText
import markdown as mark
import getpass


def mail(username=None, sender_email=None,
         subject=None, message=None,
         copy_emails=None, target_emails=None, html=False,
         markdown=False, password=None,
         really_send=True):

    if markdown:
        msg = MIMEText(mark.markdown(message), 'html')
    elif html:
        msg = MIMEText(message, 'html')
    else:
        msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = sender_email
    msg['Cc'] = ",".join(copy_emails)
    msg['To'] = ",".join(target_emails)
    print(msg)

    # prepare to send
    # Create a secure SSL context
    print('connecting')
    s = smtplib.SMTP('mail.epfl.ch', 587)
    s.starttls()
    if username is None:
        username = input('login:')
    elif password is None:
        print('login:', username)
        password = getpass.getpass()
    s.login(username, password)

    # print(sender, sender_email)
    if really_send:
        s.sendmail(sender_email,
                   sender_email, msg.as_string())

    # close connection to smtp
    s.quit()

    return msg
