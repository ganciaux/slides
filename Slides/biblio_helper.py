#!/usr/bin/env python
################################################################
import bibtexparser
from IPython.display import HTML
from IPython.display import display
import markdown
import os
################################################################


class Entry:

    def __init__(self, entry):
        self.entry = entry.copy()
        self._format()
        self.count_cited = 0

    def _format(self):
        _authors = self.entry['author'].split(' and ')
        authors = []
        for a in _authors:
            names = a.split(',')
            lastname = names[0]
            if lastname.startswith("family="):
                lastname = lastname.replace('family=', '')
            _firstnames = [e.strip() for e in names[1:]]
            __firstnames = []
            for f in _firstnames:
                if f.startswith("given="):
                    __firstnames.append(f.replace("given=", ''))
                elif f.startswith("useprefix="):
                    continue
                elif f.startswith("prefix="):
                    f = f.replace("prefix=", "")
                    lastname = f.capitalize() + " " + lastname
                else:
                    __firstnames.append(f)

            # print(lastname, __firstnames, _firstnames)

            _firstnames = __firstnames

            firstnames = []
            for f in _firstnames:
                f = f.split('-')
                firstnames += [e[0] for e in f]
            firstnames = '.-'.join(firstnames) + '.'
            firstnames = self._clean_string(firstnames)
            lastname = self._clean_string(lastname)
            authors.append(f'{firstnames} {lastname}')

        self.entry['author'] = ", ".join(authors)

    def __repr__(self):
        r = ''
        for k, v in self.entry.items():
            r += f'{k}: {v}\n'
        return r

    def _clean_string(self, _str):
        _str = _str.replace('{{', '').replace('}}', '')
        _str = _str.replace('{', '').replace('}', '')
        _str = _str.replace(r'\'e', 'é')
        _str = _str.replace(r'\"e', 'ë')
        _str = _str.replace(r'\textendash', '-')
        _str = _str.replace(r'\vz', 'ž')
        _str = _str.replace(r'\&', '&')
        return _str

    def style(self, res):
        res = '<div style="background: #ECECEC;border-radius: 5px;;padding: 1px;padding-left: 10px;">' + res + '</div>'
        return res

    def _year(self):
        year = None
        if 'date' in self.entry:
            year = self.entry['date'].split('-')[0]
        elif 'year' in self.entry:
            year = self.entry['year'].split('-')[0]
        if year is None:
            raise RuntimeError(f'no year for entry: {self}')
        year = int(year)
        return year

    def _cite(self, style=None, _doi=False):
        authors = self._clean_string(self.entry['author'])
        title = self._clean_string(self.entry['title'])
        ref = ''
        if self.entry['ENTRYTYPE'] == 'article':
            if 'journaltitle' in self.entry:
                journal = self._clean_string(self.entry['journaltitle'])
            elif 'journal' in self.entry:
                journal = self.entry['journal']
            volume = self._clean_string(self.entry['volume'])
            ref += f'{journal}. **{volume}**'

            if 'number' in self.entry:
                number = self._clean_string(self.entry['number'])
                ref += f'({number})'
            ref += ','
            if 'pages' in self.entry:
                pages = self.entry['pages'].replace('--', '-')
                ref += f'{pages}.'
            ref += f' ({self._year()})'
        if self.entry['ENTRYTYPE'] == 'book':
            publisher = self._clean_string(self.entry['publisher'])
            year = self._year()
            ref += f'({self._clean_string(publisher)}, {year}).'
        if self.entry['ENTRYTYPE'] == 'phdthesis':
            school = self._clean_string(self.entry['school'])
            year = self._year()
            ref += f' {self._clean_string(school)}, {year}.'

        if 'doi' in self.entry and _doi is True:
            ref += f" [{self.entry['doi']}](https://doi.org/{self.entry['doi']})"
        res = markdown.markdown(
            f'**{authors}**. *{title}*. {ref}')
        if style is None:
            style = self.style
        res = style(res)
        self.count_cited += 1
        display(HTML(res))

    def __dir__(self):
        _dir = ['cite'] + list(self.entry.keys())
        print(_dir)
        return _dir

    def __getattr__(self, key):
        if key == 'cite':
            return self._cite()
        try:
            ret = self.entry[key]
            return ret
        except Exception as err:
            raise AttributeError from err


################################################################


class Biblio:

    def __init__(self, fname):
        self._fname = fname
        self._time = os.path.getmtime(fname)
        self._load()

    def _load(self):
        with open(self._fname) as bibtex_file:
            bibtex_str = bibtex_file.read().encode('utf8')
            _database = bibtexparser.loads(bibtex_str)
            self._bib_database = {}
            for e in _database.entries:
                key = e['ID'].lower().replace('-', '_')
                if key in self._bib_database:
                    raise RuntimeError(f'Duplicate entry {key}')
                self._bib_database[key] = Entry(e)

    def __repr__(self):
        return (f"{self._fname}: {len(self._bib_database.keys())} "
                "biblio entries")

    def __dir__(self):
        _time = os.path.getmtime(self._fname)
        if _time > self._time:
            self._time = _time
            self._load()
        return [e + '.cite' for e in self._bib_database.keys()]

    def __getattr__(self, key):
        try:
            # ret = Entry(self._bib_database.entries[self._dict_keys[key]])
            ret = self._bib_database[key]
            return ret
        except Exception as err:
            raise AttributeError from err

    def make_biblio(self):
        _l = []
        for _id, e in self._bib_database.items():
            if e.count_cited:
                _l.append((e._year(), e))

        _l.sort(key=lambda a: a[0])
        for y, e in _l:
            e._cite(_doi=True)

################################################################


def load_bibtex(fname='bibliography.bib'):
    return Biblio(fname)
