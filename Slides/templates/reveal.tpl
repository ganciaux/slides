{% from 'mathjax.tpl' import mathjax %}

{%- macro init_reveal(reveal_prefix) -%}
  <!-- Load and configure reveal -->
  <script src="js-markdown-extra.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.10/require.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

  <!-- General and theme style sheets -->
  <link rel="stylesheet" href="{{reveal_prefix}}/css/reveal.css">
  <link rel="stylesheet" href="{{reveal_prefix}}/css/theme/simple.css" id="theme">

  <!-- Loading the mathjax macro -->
  {{ mathjax() }}
  
  <!-- Get Font-awesome from cdn -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css">

     
  <!-- End of reveal parts -->
{%- endmacro %}



{%- macro load_reveal(reveal_prefix) -%}

  <script>
    
    require(
    {
    // it makes sense to wait a little bit when you are loading
    // reveal from a cdn in a slow connection environment
    waitSeconds: 1
    },
    [
    "{{reveal_prefix}}/lib/js/head.min.js",
    "{{reveal_prefix}}/js/reveal.js"
    ],
    
    function(head, Reveal){
        
      Reveal.initialize({
      //width: '1200px',
      //height: '100%'
      //  margins: 0.1,
      controls: true,
      progress: true,
      history: true,
      transition: 'fade',
      margin: 0.02,
      progress: true,
      slideNumber: true,
      // Optional libraries used to extend on reveal.js plugins
      dependencies: [
      { src: '{{reveal_prefix}}/lib/js/classList.js',
        condition: function() { return !document.body.classList; }
      },
      { src: '{{reveal_prefix}}/plugin/markdown/marked.js',
        condition: function() { return !!document.querySelector( '[data-markdown]' ); }
      },
      { src: '{{reveal_prefix}}/plugin/markdown/markdown.js',
        condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
      { src: '{{reveal_prefix}}/plugin/highlight/highlight.js',
        async: true, callback: function() { hljs.initHighlightingOnLoad(); }
      },
      { src: '{{reveal_prefix}}/plugin/zoom-js/zoom.js', async: true },
      { src: '{{reveal_prefix}}/plugin/notes/notes.js',
        async: true, condition: function() { return !!document.body.classList; }
      }]});
    
      var update = function(event){
        if(MathJax.Hub.getAllJax(Reveal.getCurrentSlide())){
          MathJax.Hub.Rerender(Reveal.getCurrentSlide());
        }
      };
    
      Reveal.addEventListener('slidechanged', update);
    }
    );
    
    $(document).ready(function(){
    
    //$(".cell").css("width","90%");
    //$(".cell").css("margin","0 auto");
    //$(".output_area").css("text-align","center");
    //$(".output_png").css("margin","0 auto");
    //$("img").css("margin","0 auto");
    //$(".output_html").css("margin", "0 auto");
    //$(".output_latex").css("margin", "0 auto");
    //$(".output_subarea").css("flex", "None");
    
    $( "markdown" ).each(function( index ) {
    $( this ).html(Markdown($( this ).text()));
    });
    })
  </script>



{%- endmacro %}
