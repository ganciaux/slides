import yaml
import git
import pandas as pd
import os
import datetime


def get_git_root():
    path = os.getcwd()
    git_repo = git.Repo(path, search_parent_directories=True)
    git_root = git_repo.git.rev_parse("--show-toplevel")
    return git_root


def get_class_config_filename():
    root_path = get_git_root()
    config = os.path.join(root_path, 'class_config.yaml')
    if os.path.exists(config):
        return config
    else:
        raise RuntimeError(
            "This repository do not contain class/lecture information")


def parse_student_list(fname):
    student_list = pd.read_csv(
        fname, sep='|', skipinitialspace=True,
        header=0,
        usecols=lambda name: not name.startswith("Unnamed"))

    student_list.columns = student_list.columns.str.strip()
    student_list.drop(columns=student_list.columns[0], inplace=True)
    student_list.drop(index=0, inplace=True)
    student_list.drop(student_list.tail(1).index, inplace=True)
    student_list["Sciper ID"] = student_list["Sciper ID"].astype(int)
    return student_list


def parse_group_list(groups_filename):
    group_list = pd.read_csv(
        groups_filename, sep='|', skipinitialspace=True,
        header=0,
        usecols=lambda name: not name.startswith("Unnamed"))

    group_list.columns = group_list.columns.str.strip()
    group_list.drop(columns=group_list.columns[0], inplace=True)
    group_list.drop(index=0, inplace=True)
    return group_list


def get_class_config():
    fname = get_class_config_filename()
    with open(fname) as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)

    student_list_file = os.path.join(get_git_root(), config['students'])
    config['students'] = parse_student_list(student_list_file)

    for k, v in config['homeworks'].items():
        v['groups'] = parse_group_list(
            os.path.join(get_git_root(), v['groups']))

    config['git_root'] = get_git_root()
    return config


def archive_email(message_filename, msg=None):
    root_path = get_git_root()
    # create the archive
    os.system('mkdir -p archives')
    date = datetime.date.today()
    file_name = os.path.join(
        root_path, 'archives',
        os.path.basename(message_filename) + '.' + str(date))

    _f = open(file_name, 'w')
    _f.write(str(msg))
    _f.close()
