#! /usr/bin/python3

import matplotlib.pyplot as plt

################################################################


def generateFigure(fig, name, caption=None, width=1):

    if type(fig) is not list:
        fig = [fig]
    if type(name) is not list:
        name = [name]
    if type(width) is not list:
        width = [width]

    res = '{\\center'
    for f, n, w in zip(fig, name, width):
        f.savefig(n + ".png")

        if type(w) == float or type(w) == int:
            w = str(w) + "\\textwidth"

            res += "\\includegraphics" + "[width=" + w + "]{" + n + ".png}"

    res += '}'
    if caption is not None:
        res += "\\caption{"
        res += caption
        res += "}"
    return res

################################################################


def make2Dplot(np_x, np_y, fig=None, axe=None, label=None, xlabel=None, ylabel=None, loglog_flag=False, **kwargs):

    if fig is None:
        fig = plt.figure()
    if axe is None:
        axe = fig.add_subplot(1, 1, 1)

    if xlabel is not None:
        axe.set_xlabel(xlabel)
    if ylabel is not None:
        axe.set_ylabel(ylabel)

    if loglog_flag is False:
        _plot = axe.plot
    if loglog_flag is True:
        _plot = axe.loglog

    if label is not None:
        _plot(np_x, np_y, label=label, **kwargs)
        axe.legend(loc='best')
    else:
        _plot(np_x, np_y, **kwargs)

    return fig, axe

################################################################


def generateArticle():
    import os
    path = os.path.dirname(__file__)
    f = open(os.path.join(path, './article.header'))
    return f.read()

################################################################


def generateBeamer():
    import os
    path = os.path.dirname(__file__)
    f = open(os.path.join(path, './beamer.header'))
    return f.read()


flags = {}
flags['slide_open'] = False


def closeSlide():
    flags['slide_open'] = False
    return "\\end{frame}\n"


def newSlide(title, subtitle=None):
    res = ""
    if flags['slide_open']:
        res += closeSlide()

    flags['slide_open'] = True
    res += """\\begin{frame}
\\frametitle{""" + '{0}'.format(title) + "}\n"
    if subtitle is not None:
        res += """\\framesubtitle{""" + '{0}'.format(subtitle) + '}\n\n'
    return res
