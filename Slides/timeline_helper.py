#!/usr/bin/env python
################################################################

import matplotlib.pyplot as plt
from datetime import date
import numpy as np
from datetime import timedelta
################################################################


def display_timeline_horizontal(labels, dates, title=None):

    min_date = date(np.min(dates).year - 2,
                    np.min(dates).month, np.min(dates).day)
    max_date = date(np.max(dates).year + 2,
                    np.max(dates).month, np.max(dates).day)

    fig, ax = plt.subplots(figsize=(15, 4), constrained_layout=True)
    _ = ax.set_ylim(-2, 1.75)
    _ = ax.set_xlim(min_date, max_date)
    _ = ax.axhline(0, xmin=0.05, xmax=0.95, c='deeppink', zorder=1)

    _ = ax.scatter(dates, np.zeros(len(dates)),
                   s=120, c='palevioletred', zorder=2)
    _ = ax.scatter(dates, np.zeros(len(dates)),
                   s=30, c='darkmagenta', zorder=3)

    label_offsets = np.zeros(len(dates))
    label_offsets[::2] = 0.35
    label_offsets[1::2] = -0.7
    for i, (l, d) in enumerate(zip(labels, dates)):
        _ = ax.text(d, label_offsets[i], l, ha='center', fontfamily='serif',
                    fontweight='bold', color='royalblue', fontsize=12)

    stems = np.zeros(len(dates))
    stems[::2] = 0.3
    stems[1::2] = -0.3
    markerline, stemline, baseline = ax.stem(
        dates, stems)
    _ = plt.setp(markerline, marker=',', color='darkmagenta')
    _ = plt.setp(stemline, color='darkmagenta')

    # hide lines around chart
    for spine in ["left", "top", "right", "bottom"]:
        _ = ax.spines[spine].set_visible(False)

    # hide tick labels
    _ = ax.set_xticks([])
    _ = ax.set_yticks([])

    if title is not None:
        _ = ax.set_title(title,
                         fontweight="bold", fontfamily='serif', fontsize=16,
                         color='royalblue')

    return fig

################################################################


def display_timeline_vertical(labels, dates, title=None):

    min_date = date(np.min(dates).year - 2,
                    np.min(dates).month, np.min(dates).day)
    max_date = date(np.max(dates).year + 2,
                    np.max(dates).month, np.max(dates).day)

    fig = plt.figure(figsize=(6, 10))
    ax = plt.subplot(111)
    _ = ax.set_xlim(-20, 20)
    _ = ax.set_ylim(min_date, max_date)
    _ = ax.axvline(0, ymin=0.05, ymax=0.95, c='deeppink', zorder=1)

    _ = ax.scatter(np.zeros(len(dates)), dates,
                   s=120, c='palevioletred', zorder=2)
    _ = ax.scatter(np.zeros(len(dates)), dates,
                   s=30, c='darkmagenta', zorder=3)

    label_offsets = np.repeat(2.0, len(dates))
    label_offsets[1::2] = -2.0
    for i, (l, d) in enumerate(zip(labels, dates)):
        d = d - timedelta(days=90)
        align = 'right'
        if i % 2 == 0:
            align = 'left'
        _ = ax.text(label_offsets[i], d, l, ha=align, fontfamily='serif',
                    fontweight='bold', color='royalblue', fontsize=12)

    stems = np.repeat(2.0, len(dates))
    stems[1::2] *= -1.0
    _ = ax.hlines(dates, 0, stems, color='darkmagenta')

    # hide lines around chart
    for spine in ["left", "top", "right", "bottom"]:
        _ = ax.spines[spine].set_visible(False)

    # hide tick labels
    _ = ax.set_xticks([])
    _ = ax.set_yticks([])

    if title is not None:
        _ = ax.set_title(title,
                         fontweight="bold", fontfamily='serif', fontsize=16,
                         color='royalblue')

    return fig


################################################################
# import pandas as pd
# import datetime
# source = pd.DataFrame([
#     {"drama": "Pride and Prejudice", "start": '1795-01-01', "end": '1810-01-01'},
#     {"drama": "Sense and Sensibility", "start": '1792-01-01', "end": '1797-01-01'},
#     {"drama": "Jane Eyre", "start": '1799-01-01', "end": '1819-01-01'},
#     {"drama": "Bridgerton", "start": '1813-01-01', "end": '1827-01-01'},
#     {"drama": "Middlemarch", "start": '1829-01-01', "end": '1832-01-01'},
#     {"drama": "Cranford", "start": '1842-01-01', "end": '1843-01-01'},
#     {"drama": "David Copperfield", "start": '1840-01-01', "end": '1860-01-01'},
#     {"drama": "Poldark", "start": '1781-01-01', "end": '1801-01-01'},
#     {"drama": "North and South", "start": '1850-01-01', "end": '1860-01-01'},
#     {"drama": "Barchester Chronicles", "start": '1855-01-01', "end": '1867-02-01'},
#     {"drama": "The Way We Live Now", "start": '1870-01-01', "end": '1880-02-01'},
#     {"drama": "Tess of the D’Urbervilles",
#         "start": '1880-01-01', "end": '1890-02-01'},
#     {"drama": "Upstairs, Downstairs", "start": '1903-01-01', "end": '1930-02-01'},
#     {"drama": "Downton Abbey", "start": '1912-01-01', "end": '1939-02-01'},
#     {"drama": "Jewel in the Crown", "start": '1942-01-01', "end": '1947-02-01'},
#     {"drama": "Call the Midwife", "start": '1957-01-01', "end": '1967-02-01'},
#
# ])
#
# display_timeline_vertical(
#     source['drama'], source['end'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d')))
# plt.show()
