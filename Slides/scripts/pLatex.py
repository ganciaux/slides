#! /bin/env python

import subprocess
import os.path
import argparse
from Slides.PythonLatex.generate_latex import interpretPython
################################################################


def main():

    parser = argparse.ArgumentParser(
        description='Python embedded in Latex tool')
    parser.add_argument('filename_in', type=str,
                        help='The path of the file to parse')
    parser.add_argument('--reexec_flag', action='store_true', help='TODO')
    parser.add_argument('-g', '--generate_pdf', action='store_true',
                        help='Ask to generate the pdf directly')
    parser.add_argument('-c', '--continue_flag', action='store_true',
                        help='Ask to continue even after errors')
    args = parser.parse_args()
    filename_in = args.filename_in
    reexec_flag = args.reexec_flag
    _continue = args.continue_flag
    generate_pdf = args.generate_pdf
    basename, ext = os.path.splitext(filename_in)
    if not ext == '.ptex':
        raise Exception(
            'invalid file extension: "{0}" in file {1}'.format(ext, filename_in))
    filename_out = basename + '.tex'
    ################################################################
    out = interpretPython(filename_in, _continue, reexec_flag)
    fout = open(filename_out, 'w')
    fout.write(out)
    fout.close()
    log = open('log.latex', 'w')

    if generate_pdf is True:
        ret = subprocess.call(
            'pdflatex -interaction nonstopmode ' +
            filename_out, shell=True,
            stdout=log, stderr=log)
        if ret:
            subprocess.call('rubber -d ' + filename_out, shell=True)
################################################################


if __name__ == "__main__":
    main()
