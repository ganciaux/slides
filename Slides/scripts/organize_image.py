#!/bin/env python

"""
This executable permits to organize the image
links in a Jupyter notebook.
"""
################################################################
import argparse
import Slides.cell_manager as cell_manager
import os
import pyparsing as pp
################################################################
list_images = set()


def populate_list_images(image_dir):
    if not os.path.isdir(image_dir):
        os.mkdir(image_dir)
    for i in os.listdir(image_dir):
        i = os.path.join(image_dir, i)
        i = os.path.abspath(i)
        i = os.path.relpath(i, os.getcwd())
        list_images.add(i)
################################################################
# parsing rules for attachments


def _print(label):
    return lambda x: print(label + ':', x)


valid_characters = pp.printables
for c in "[]!()":
    valid_characters = valid_characters.replace(c, '')

markdown_image_link = (pp.Literal("![") +
                       pp.Word(valid_characters) +
                       pp.Literal("]") +
                       pp.Literal("(") +
                       pp.Word(valid_characters) +
                       pp.Literal(")"))

markdown_image_link.addParseAction(
    lambda x: {'alt': x[1].strip(), 'src': x[4].strip(),
               'link': ''.join(x)})

valid_characters = pp.printables
for c in "<>\"='":
    valid_characters = valid_characters.replace(c, '')
valid_characters += ' '


def html_image_option(name):
    opt = pp.Literal(f'{name}="') + pp.Word(valid_characters) + pp.Literal('"')
    opt.addParseAction(
        lambda _, b, x: {name: x[1].strip()})
    opt2 = pp.Literal(f'{name}=\'') + \
        pp.Word(valid_characters) + pp.Literal('\'')
    opt2.addParseAction(
        lambda _, b, x: {name: x[1].strip()})

    return opt | opt2


html_image_body = pp.OneOrMore(html_image_option('alt') |
                               html_image_option('src') |
                               html_image_option('width'))
html_image_link = (pp.Literal("<img") +
                   html_image_body + pp.Literal(">").addParseAction(
                       lambda _, b, x: {'end': b}))


def gather_parsed(original, _l, tokens):
    res = {}
    end = tokens[-1]['end']+1
    res['link'] = original[_l:end]
    for e in tokens[1:-1]:
        res.update(e)
    return res


html_image_link.addParseAction(gather_parsed)

image_link = html_image_link | markdown_image_link

images = pp.ZeroOrMore(
    pp.SkipTo(image_link).suppress() + image_link)

################################################################


def create_image_file(fname, data, image_dir=None):
    try:
        os.mkdir(image_dir)
    except FileExistsError:
        pass

    base, ext = os.path.splitext(fname)
    fname = os.path.join(base.replace(' ', '_') + ext)
    fname = os.path.join(image_dir, fname)
    while os.path.exists(fname):
        base, ext = os.path.splitext(fname)
        fname = os.path.join(base+'_' + ext)

    with open(fname, 'wb') as f:
        import base64
        data = base64.b64decode(data)
        f.write(data)
    return fname

################################################################


def extract_attachement(cell, **kwargs):
    cited_attachments = {}
    links = images.parseString(cell.source)
    for _l in links:
        fname = _l['src']
        if not fname.startswith('attachment:'):
            continue
        fname = fname.replace('attachment:', '')
        cited_attachments[fname] = _l

    for name, a in cell.attachments.items():
        for _type, _file in a.items():
            if name in cited_attachments:
                print(f'found_matching_attachment: {name}: {_type}')
                fname = create_image_file(name, _file, **kwargs)
                att = cited_attachments[name]
                print(att)
                cell.source = cell.source.replace(
                    att['link'],
                    f'<img alt="{att["alt"]}" src="./{fname}">')


################################################################

def check_images(cell, **kwargs):
    links = images.parseString(cell.source)

    if links:
        print(cell.source)
        print([e['src'] for e in links])

    for _l in links:
        src = _l['src']
        if not os.path.exists(src):
            raise RuntimeError(f"not existing image: {src}")


################################################################
cited_images = []


def list_images_links(cell, **kwargs):
    print(cell.source)
    links = images.parseString(cell.source)
    global cited_images
    print(links)
    cited_images += [e['src'] for e in links]


################################################################


def main():
    parser = argparse.ArgumentParser(description='Jupyter image organizer')
    parser.add_argument("--image_dir", type=str, default='images',
                        help='Specify the directory where to store images')
    parser.add_argument("notebooks", nargs='*',
                        help="The notebooks to associate in a presentation")
    parser.add_argument("-o", "--out_filename", type=str, default="talk.ipynb",
                        help="name of notebook to be generated")
    parser.add_argument("--extract_attachements", action='store_true',
                        help="demands to extract attachements images")
    parser.add_argument("--check_images", action='store_true',
                        help="check if all images are present")
    parser.add_argument("--clean_images", action='store_true',
                        help="delete unused images")

    arguments = parser.parse_args()

    populate_list_images(arguments.image_dir)

    def analyze_images(i, cell):
        # print(f'{i}: {list(cell.keys())}')
        if 'attachments' in cell and arguments.extract_attachements:
            extract_attachement(cell, image_dir=arguments.image_dir)
        if arguments.check_images:
            check_images(cell, image_dir=arguments.image_dir)
        if arguments.clean_images:
            list_images_links(cell, image_dir=arguments.image_dir)
        return cell

    try:
        cell_manager.mergeNotebooks(arguments.notebooks,
                                    modify_functor=analyze_images,
                                    out_filename=arguments.out_filename)

    except cell_manager.NotFoundNotebooks:
        print('Could not find the notebooks to treat: check path provided')
        return

    global cited_images
    cited_images = [os.path.abspath(e) for e in cited_images]
    cited_images = [os.path.relpath(e, os.getcwd()) for e in cited_images]
    print(cited_images)
    if arguments.clean_images:
        print(f'found {len(list_images)} image files')
        print(f'found {len(cited_images)} cited images')
        for fname in list_images:
            if fname not in cited_images:
                print(f"should_delete_file: {fname}")


################################################################
if __name__ == "__main__":
    main()
