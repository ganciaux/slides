#!bin/env python

"""
This executable permits to generate an advanced presentation
from a Jupyter notebook.

The capabilities are:
- a server of reveal content
- a pptx (under developement)
- a beamer tex file (TODO)
"""
################################################################
import argparse
import os
import re
import Slides.count_slides as count_slides
import Slides.cell_manager as cell_manager
################################################################


def getNotebookFilenames(slides_file):
    """
    When a directory is provided to the executable,
    a file named 'slides' is searched for with
    the expected list of ipython notebooks.
    The list of notebooks to consider is then
    returned.
    """
    if not os.path.isfile(slides_file):
        raise Exception("Cannot find slides file")

    f = open(slides_file)
    slides = [_l.strip() for _l in f.readlines() if _l != ""]
    return slides
################################################################


def main():
    parser = argparse.ArgumentParser(description='Slide launcher')
    parser.add_argument(
        "--execute", action='store_true',
        help=('Requests to execute all the notebooks launching '
              'the slides server'))
    parser.add_argument("--no_count", action='store_false',
                        help="Requests not to count slides generated")
    parser.add_argument("notebooks", nargs='*',
                        help="The notebooks to associate in a presentation")
    parser.add_argument(
        "--internet", action='store_true',
        help="Weather the http server should serve on the internet")
    parser.add_argument("--html", action='store_true',
                        help="only produce a html file")
    parser.add_argument("--beamer", action='store_true',
                        help="produce a beamer/latex file and the pdf")
    parser.add_argument("--add_cell_number", action='store_true',
                        help="add the cell number before each cell")
    parser.add_argument("--select_cells", type=int, nargs='*',
                        help="Provides a list of cells to select")
    parser.add_argument("--font_size", type=str, default='25px',
                        help="Provides the font size of the presentation")

    parser.add_argument("-o", "--out_filename", type=str, default="talk.ipynb",
                        help=("Gives the name of notebook to generate when"
                              " transforming or merging input notebooks"))

    parser.add_argument("-f", "--filter_correction", action="store_true",
                        help=("Asks to produce a copy of the notebook with "
                              " the correction removed"))
    parser.add_argument("-p", "--prepare_correction", action="store_true",
                        help=("Asks to produce a copy of the note book with "
                              " only the correction tags removed"))

    arguments = parser.parse_args()
    filtered_replacement = None

    if len(arguments.notebooks) == 1:
        if os.path.isdir(arguments.notebooks[0]):
            slides_path = arguments.notebooks[0]
            slides_file = os.path.join(slides_path, 'slides')
            arguments.notebooks = getNotebookFilenames(slides_file)
        else:
            _file = arguments.notebooks[0]
            ext = os.path.splitext(_file)[1]
            if ext == '.sld':
                arguments.notebooks = getNotebookFilenames(_file)

    if arguments.execute:
        for n in arguments.notebooks:
            cell_manager.executeNotebook(n)

    select_functor = None
    modify_functor = None

    if arguments.select_cells:
        select_cells = set(arguments.select_cells)

        def _sel_by_index(i, _cell):
            return i in select_cells

        select_functor = _sel_by_index

    if arguments.filter_correction:
        # _original_sel = select_functor
        def _sel_correction(__unused__i, cell):
            # if _original_sel(i, cell) is False:
            #    return False
            if ("solution" in cell.metadata and
                    cell.metadata.solution is True):
                cell.source = "**Place your answer here**"
                cell.output = ""
            if re.search("SolutionToRemove", cell.source):
                if cell.cell_type == 'code':
                    cell.source = "# Place your answer here"
                if cell.cell_type == 'markdown':
                    cell.source = '---\n\n **Place your answer here** \n\n ---\n\n'
                # print(cell.source)
            return cell
        modify_functor = _sel_correction

    if arguments.prepare_correction:
        def _remove_solution_tags(__unused__i, cell):
            # print(cell)
            if cell.cell_type == 'markdown':
                cell.source = cell.source.replace(
                    "SolutionToRemove", "---\n\n **Solution:**\n\n ---\n\n").strip()
            if cell.cell_type == 'code':
                cell.source = cell.source.replace(
                    "# SolutionToRemove", "###########\n# Solution:\n##########\n").strip()
            # print(cell)
            return cell

        modify_functor = _remove_solution_tags

    try:
        cell_manager.mergeNotebooks(arguments.notebooks,
                                    add_cell_number=arguments.add_cell_number,
                                    select_functor=select_functor,
                                    modify_functor=modify_functor,
                                    out_filename=arguments.out_filename)

    except cell_manager.NotFoundNotebooks:
        print('Could not find the notebooks to treat: check path provided')
        return

    if not arguments.no_count:
        nb_slides = count_slides.countSlides([arguments.out_filename])
        print("*" * 20)
        print("You generated a talk with {0} slides".format(nb_slides))
        print("*" * 20)

    if arguments.filter_correction or arguments.prepare_correction:
        return

    if arguments.html:
        cell_manager.generateHTML(
            arguments.out_filename, font_size=arguments.font_size)

    elif arguments.beamer:
        cell_manager.generateBeamer(
            arguments.out_filename, font_size=arguments.font_size)

    elif arguments.internet:
        cell_manager.launchSlideServer(arguments.out_filename,
                                       options="--ServePostProcessor.ip='*'",
                                       font_size=arguments.font_size)

    else:
        cell_manager.launchSlideServer(
            arguments.out_filename, font_size=arguments.font_size)

################################################################


if __name__ == "__main__":
    main()
