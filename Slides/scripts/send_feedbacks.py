#!/bin/env python

import Slides.mail_helper as mailer
import argparse
import markdown
import getpass
import os
import time

import Slides.class_helper as ch


def main():

    config = ch.get_class_config()

    parser = argparse.ArgumentParser(
        description='Send feedbacks of homework')

    parser.add_argument('-n', '--homework', type=str,
                        help='specify the homework label',
                        required=True)

    parser.add_argument('-u', '--username', type=str,
                        help='Username to log to the SMTP server')

    parser.add_argument('-t', '--target', type=str,
                        help='Specify the name of a mail box'
                        ' to send test messages,'
                        'default : {config["teachers"][0]}',
                        default=config['teachers'][0])

    args = parser.parse_args()
    students_list = config['students']
    students_list.rename(columns={"e-Mail": "email"},
                         inplace=True)
    students_list = students_list.applymap(
        lambda x: x.strip() if isinstance(x, str) else x)

    students_list.set_index("email", inplace=True)

    homework = config['homeworks'][args.homework]
    group_list = homework['groups']

    ################################################################
    # find feedback file

    feed_back = os.path.join(config['git_root'],
                             'homeworks', args.homework+'.report.org')
    if not os.path.exists(feed_back):
        print(
            f"Warning: cannot find the report file: {feed_back}")

    target = args.target
    username = args.username

    if username:
        print('login:', username)
        password = getpass.getpass()

    ################################################################
    # output info

    print('total_students:', len(students_list))
    print('nb_groups:', len(group_list))

    ################################################################

    def get_group_emails(group):
        emails = group.split(' ')[0].split('_')
        for e in emails:
            if e not in students_list.index:
                print(emails)
                raise RuntimeError(
                    "student email not found in student list:"
                    f" {e}\n{students_list.index}")

        return emails

    def get_group_corrector(group):
        corrector = group.split(' ')[1].strip('()')
        return corrector

    def treat_comments(group, comments):
        lines = comments.split('\n')
        print(group)
        emails = get_group_emails(group)
        emails = ' & '.join(emails)
        header = '# Group: ' + emails
        # commit_info = '## git_commit: ' + lines[0]
        commit_info = ''
        corrector = '## Corrector: ' + get_group_corrector(group)
        comments = '\n'.join(lines[1:]).replace(
            '---', '\n').replace('###', '\n### ').replace(
                'General comments:', 'General comments:\n\n').replace(
                    '\n\n\n', '\n\n')
        return (header + '\n' + corrector + '\n' +
                commit_info + '\n' + comments)

    ################################################################

    feed_back = open(feed_back).read()
    feed_back = [s.strip() for s in feed_back.split('* ')
                 if s.strip() != '']

    feed_backs = dict()
    key = None
    val = None
    for i, f in enumerate(feed_back):
        if i % 2 == 0:
            key = f.replace('\n*', '')
        if i % 2 == 1:
            val = f
            feed_backs[key] = treat_comments(key, val)

    ################################################################
    feedback_message = config['feedback_template']

    messages = dict()
    for k, v in feed_backs.items():
        messages[k] = markdown.markdown(feedback_message) + '\n' + v

    ################################################################

    if username:
        for k, m in messages.items():
            # print('send to:', get_group_emails(k))
            mailer.mail(
                markdown=True,
                username=username,
                sender_email=target,
                subject=f'{config["acronym"]} homework {args.homework} feedback',
                message=m,
                copy_emails=config['teachers']+config['assistants'],
                target_emails=get_group_emails(k),
                password=password
            )
            time.sleep(1)
    else:
        for k, m in messages.items():
            print('send to:', get_group_emails(k))
            print(m)
            # print(markdown.markdown(m))

################################################################


if __name__ == "__main__":
    main()
