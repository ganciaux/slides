#!/bin/env python

import argparse
import random
import time
import sys
import os
import signal
import Slides.class_helper as ch
################################################################


def signal_handler(sig, frame):
    os.system('reset')
    sys.exit(0)


def main():

    signal.signal(signal.SIGINT, signal_handler)

    ################################################################

    config = ch.get_class_config()

    parser = argparse.ArgumentParser(description='Mail to class helper')
    students = config['students']

    # #### read the emails
    students = [student for i, student in students.iterrows()]

    chosen_student = random.choice(students)
    chosen_name = chosen_student['Official name'].strip()
    chosen_email = chosen_student['e-Mail'].strip()

    os.system('reset')
    print('Winner is: ', end='')
    sys.stdout.flush()

    for i in range(10):
        time.sleep(.1)
        print('=', end='')
        sys.stdout.flush()

    print('> ', chosen_name + ' (' + chosen_email + ')')
    time.sleep(60)


################################################################

if __name__ == "__main__":
    main()
