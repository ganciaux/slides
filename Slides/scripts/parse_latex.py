#! /bin/env python

import os.path
import argparse
import Slides.PythonLatex.latex_structure as tex
################################################################


def main():
    parser = argparse.ArgumentParser(
        description='Python embedded in Latex tool')
    parser.add_argument('filename_in', type=str,
                        help='The path of the file to parse')
    args = parser.parse_args()
    filename_in = args.filename_in
    basename, ext = os.path.splitext(filename_in)
    if not ext == '.ptex':
        raise Exception(
            f'invalid file extension: "{ext}" in file {filename_in}')

    global indentation
    indentation = 0

    def explore(block):
        global indentation

        for i, b in enumerate(block.content):
            if type(b) == tex.LatexEnvironment:
                print("  "*indentation, i, b.name)
                print("  "*indentation, '######')
                indentation += 1
                explore(b)
                indentation -= 1
                print("  "*indentation, '######')
            else:
                txt = str(b).strip()
                if txt != "":
                    print("  "*indentation, i, type(b))
                    print("  "*indentation, '######')
                    print(txt)
                    print("  "*indentation, '######')

    tex_struct = tex.LatexStructure()
    tex_struct.parseLatexFile(filename_in)
    explore(tex_struct)

################################################################


if __name__ == "__main__":
    main()
