#!/bin/env python
################################################################
import argparse
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE_TYPE, PP_PLACEHOLDER
import os
################################################################


class PPTXParser:
    def __init__(self, filename):
        self.prs = Presentation(filename)

    def _title(self, title):
        print(f'#{self.current_slide}: found title')
        print(f'{title}')

    def _image(self, name, filename, data):
        print(f'#{self.current_slide}: found image {name} {filename}')
        print(f'{type(data)}')

    def _text(self, text, text_frame):
        print(f'#{self.current_slide}: found text')
        print(f'{text_frame.text}')

    def _slide_number(self, number):
        print(f'#{self.current_slide}: found slide number {number}')

    def _group(self, group):
        print(f'#{self.current_slide}: found group {group}')
        for s in group.shapes:
            self._parse_shape(s)

    def _placeholder(self, shape):
        phf = shape.placeholder_format
        if phf.type == PP_PLACEHOLDER.SLIDE_NUMBER:
            self._slide_number(int(shape.text))
            return
        elif phf.type == PP_PLACEHOLDER.TITLE:
            return
        elif shape.has_text_frame:
            self._text(shape.text, shape.text_frame)
        else:
            print('  %d, %s' % (phf.idx, phf.type))

    def _parse_shape(self, shape):
        s = shape
        print(
            f'#{self.current_slide}: xfound {s.name} '
            f'{s.shape_type} {type(s)}')

        if shape.is_placeholder:
            self._placeholder(shape)
        elif s.shape_type == MSO_SHAPE_TYPE.PICTURE:
            img = s.image
            self._image(s.name, img.filename, img.blob)
        elif s.shape_type == MSO_SHAPE_TYPE.TEXT_BOX:
            self._text(s.text, s.text_frame)
        elif s.shape_type == MSO_SHAPE_TYPE.GROUP:
            self._group(s)
        elif s.shape_type == MSO_SHAPE_TYPE.AUTO_SHAPE:
            print('bb', s.text)

    def parse(self):
        for i, sl in enumerate(self.prs.slides):
            self.current_slide = i
            if sl.shapes.title:
                self._title(sl.shapes.title.text)
            for s in sl.shapes:
                self._parse_shape(s)


################################################################


class ExportParser(PPTXParser):

    def _title(self, title):
        self.f.write(f'# {title.strip()}\n\n')

    def _text(self, text, text_frame):
        super()._text(text, text_frame)
        for p in text_frame.paragraphs:
            self.f.write(p.text + '\n')

    def _image(self, name, filename, data):
        try:
            os.mkdir('images')
        except FileExistsError:
            pass

        base, ext = os.path.splitext(filename)
        fname = os.path.join(name.replace(' ', '_') + ext)
        fname = os.path.join('images', fname)
        while os.path.exists(fname):
            base, ext = os.path.splitext(fname)
            fname = os.path.join(base+'_' + ext)

        self.f.write(
            f'<img alt="{name}" src="./{fname}">\n\n')

        with open(fname, 'wb') as f:
            f.write(data)

    def export(self, filename):
        self.f = open(filename, 'w')
        self.parse()


################################################################


def main():
    parser = argparse.ArgumentParser(
        description='Convert pptx to Jupyter notebook')
    parser.add_argument("filename",
                        help="The pptx file to convert into notebooks")
    parser.add_argument("--output", "-o", default='test.md',
                        help="The markdown file to produce")

    args = parser.parse_args()
    parser = ExportParser(args.filename)
    parser.export(args.output)

################################################################


if __name__ == "__main__":
    main()

# import sys
# import nbformat
# from PythonLatex.latex_structure import LatexStructure
# from PythonLatex.latex_structure import LatexEnvironment
#
# filename_in = sys.argv[1]
# filename_out = sys.argv[2]
#
# tex_struct = LatexStructure()
# tex_struct.parseLatexFile(filename_in)
# print(str(tex_struct))
#
# nb = nbformat.notebooknode.NotebookNode({
#    u'nbformat_minor': 2,
#    u'cells': [],
#    u'nbformat': 4,
#    u'metadata': {u'kernelspec': {u'display_name': u'Python 2',
#                                  u'name': u'python2',
#                                  u'language': u'python'},
#
#                  u'toc': {u'nav_menu': {u'width': u'252px',
#                                         u'height': u'12px'},
#                           u'toc_window_display': False,
#                           u'widenNotebook': False,
#                           u'toc_section_display': u'block',
#                           u'colors': {u'hover_highlight': u'#DAA520',
#                                       u'running_highlight': u'#FF0000',
#                                       u'selected_highlight': u'#FFD700'},
#                           u'navigate_menu': True,
#                           u'number_sections': True,
#                           u'threshold': 4,
#                           u'moveMenuLeft': True,
#                           u'sideBar': True,
#                           u'toc_cell': False},
#                  u'hide_input': False,
#                  u'language_info': {u'mimetype': u'text/x-python',
#                                     u'nbconvert_exporter': u'python',
#                                     u'version': u'2.7.13',
#                                     u'name': u'python',
#                                     u'file_extension': u'.py',
#                                     u'pygments_lexer': u'ipython2',
#                                     u'codemirror_mode': {
#                                         u'version': 2,
#                                         u'name': u'ipython'}}}})
#
# nb['cells'] = []
#################################################################
#
#
# def createNewCell():
#    print('create new cell')
#    new_cell = nbformat.notebooknode.NotebookNode()
#    new_cell['cell_type'] = 'markdown'
#    new_cell['source'] = ''
#    new_cell['metadata'] = {}
#    nb['cells'].append(new_cell)
#    return new_cell
#################################################################
#
#
# current_cell = None
#
# for c in tex_struct.content:
#    if current_cell is None:
#        current_cell = createNewCell()
#    if isinstance(c, LatexEnvironment):
#        current_cell = createNewCell()
#        print('AAA:', type(c), str(c))
#
#    print('adding:', type(c), str(c))
#    current_cell['source'] += str(c)
#
# print(nb)
# print(type(nb))
# nbformat.write(nb, 'test.ipynb')
