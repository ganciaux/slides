#!/bin/env python

import argparse
import Slides.mail_helper as mailer
import Slides.class_helper as ch

################################################################


def main():
    config = ch.get_class_config()

    parser = argparse.ArgumentParser(description='Mail to class helper')
    parser.add_argument('-m', '--message', type=str,
                        help='File containing the message to send',
                        required=True)

    parser.add_argument('-s', '--subject', type=str,
                        help='Specify the subject of the email',
                        required=True)

    parser.add_argument('-t', '--target', type=str,
                        help='Specify the name of a mail box'
                        ' to send test messages',
                        default="guillaume.anciaux@epfl.ch")

    parser.add_argument('-u', '--username', type=str,
                        help='Username to log to the SMTP server',
                        required=True)

    parser.add_argument('--markdown', action='store_true',
                        help='Ask to parse as markdown and generate html')

    args = parser.parse_args()
    students = config["students"]
    message_filename = args.message
    subject = args.subject
    target = args.target
    username = args.username

    teachers = config['teachers']
    assistants = config['assistants']

    # read the message to send
    message = open(message_filename).read()

    emails = list()
    # loop over students to send the email
    for i, student in students.iterrows():
        email = student['e-Mail']
        name = student['Official name']
        print("Mail to: {0}".format(email))
        emails.append(email)

    msg = mailer.mail(username=username, sender_email=target,
                      subject=subject, message=message,
                      copy_emails=teachers+assistants, target_emails=emails,
                      markdown=args.markdown)

    ch.archive_email(message_filename, msg=msg)

################################################################


if __name__ == "__main__":
    main()
