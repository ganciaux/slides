#!/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import nbformat
import re
import os

tests_path = os.path.dirname(os.path.realpath(__file__))


def test_filter_corrections():
    fname = "slides_test.ipynb"
    fname = os.path.join(tests_path, fname)

    subprocess.call(f"slides -f {fname} -o {fname}_filtered.ipynb", shell=True)
    notebook = nbformat.read(f"{fname}_filtered.ipynb", as_version=4)
    for cell in notebook.cells:
        if re.search("SolutionToRemove", cell.source):
            print(cell.source)
            raise RuntimeError("Filtering did not work properly")
    if len(notebook.cells) != 5:
        raise RuntimeError(
            f"The filtered number of cells should be 5 (it is {len(notebook.cells)})")
    os.remove(f'{fname}_filtered.ipynb')
