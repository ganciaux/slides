#!/bin/env python3
# -*- coding: utf-8 -*-

from Slides import snippet_helper as sh


def test_cpp_snippet_from_file():
    sh.Snippet.default_line_number = True
    snip = sh.CPPSnippet('tests/snippet.cpp')
    snip.save('snippet2.cpp')
    f = open('snippet2.cpp')
    snip2 = f.read()
    f.close()
    if snip.get_content() != snip2:
        print('AAA ', snip.get_content())
        print('BBB ', snip2)
        raise RuntimeError('failed test')


def test_cpp_snippet_format():
    sh.Snippet.default_line_number = True
    code = """
#include <iostream>

int main(int argc, char **argv){
        return 0;
}
        """
    code = code.replace(';\n', ';')
    snip = sh.CPPSnippet(code, format_flag=True)
    snip.save('snippet2.cpp')
    f = open('snippet2.cpp')
    snip2 = f.read()
    f.close()
    assert snip.get_content() == snip2
    assert not snip.get_content() == code


def test_cpp_snippet_compile_fail():
    sh.Snippet.default_line_number = True
    code = """
#include <iostream>

int main(int argc, char **argv){
        return 0
}
        """
    code = code.replace(';\n', ';')
    try:
        sh.CPPSnippet(code, compile_flag=True)
        raise RuntimeError("compilation error not detected")
    except sh.CPPSnippet.CompilationError:
        pass
    except Exception as e:
        raise RuntimeError('wrong exception:' + str(e))


def test_cpp_snippet_compile():
    sh.Snippet.default_line_number = True
    code = """
#include <iostream>

int main(int argc, char **argv){
        return 0;
}
        """
    code = code.replace(';\n', ';')
    try:
        sh.CPPSnippet(code, compile_flag=True)
    except Exception as e:
        raise RuntimeError('should compile: ' + str(e))
