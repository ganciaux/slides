#!/bin/env python3
# -*- coding: utf-8 -*-

from Slides import math_helper as mh
import numpy as np
from sympy import Matrix
import subprocess
import os

tests_path = os.path.dirname(os.path.realpath(__file__))


def test_print_colored_matrix():
    mh.init_printing()
    a = Matrix(np.zeros((2, 2)))
    a = mh.ColoredMatrix(a)
    mh.print_latex('{0}', a)


def test_jupyter_notebook():
    file_path = os.path.join(tests_path, "math_test.ipynb")

    ret = subprocess.call(
        "jupyter nbconvert --to notebook "
        f"--execute {file_path}",
        shell=True)
    if ret:
        raise RuntimeError("could not execute the notebook")
